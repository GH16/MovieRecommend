package com.zgh.caseclass

/**
  * 存放推荐结果
  * Created by zgh on 2019/3/7.
  */
case class Result(userId:Int,movieId:Int,rating:Double)
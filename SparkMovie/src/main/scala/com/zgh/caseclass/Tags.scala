package com.zgh.caseclass

/**
  * 用于描述信息的样例类
  * Created by zgh on 2019/2/28.
  */
case class Tags(userId:Int,movieId:Int,tag:String,timestamp:Int)

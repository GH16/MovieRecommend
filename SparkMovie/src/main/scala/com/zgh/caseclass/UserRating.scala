package com.zgh.caseclass

/**
  * 接收用户的评分信息
  * Created by zgh on 2019/3/18.
  */
case class UserRating(userId:Int, movieId:Int, rating:Double)

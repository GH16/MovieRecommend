package com.zgh.caseclass

/**
  * 用于描述信息的样例类
  * Created by zgh on 2019/2/25.
  */
case class Links(movieId:Int,imdbId:Int,tmdbId:Int)

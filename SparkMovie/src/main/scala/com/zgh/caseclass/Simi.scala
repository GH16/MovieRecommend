package com.zgh.caseclass

/**
  * 存放物品相似度
  * Created by zgh on 2019/3/7.
  */
case class Simi(itemid1:Int, itemid2:Int, similar:Double)
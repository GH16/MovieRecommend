# MovieRecommend

#### 介绍
实时电影网站观众预测系统
该项目主要由前端展示、后台管理和预测系统三个模块组成。用户在网站上点击行为，比如电影的浏览、电影评价及电影收藏等行为会在nginx 上被采集下来，然后flume对日志进行实时监听，传送给kafka，Hadoop和spark即可进行消费，进行各种预测处理，最后将用户的喜好预测结果展示给用户。

百度网盘：链接：https://pan.baidu.com/s/1vANThw59theCBd7cGoNhxQ 

提取码：abeo 

#### 软件架构
软件架构说明
开发工具 :	IntelliJ IDEA   Maven   Mysql	linux   Git

实现技术：SSM、easyui、JQuery、hadoop 、zookeeper、flume、kafka、nginx 、spark、hive、FastDFS

#### 安装教程

1. Hadoop、spark以及hive等的搭建和操作可查看网盘里的电子书教程。

2. 其他搭建请网上查找搭建。

#### 使用说明

1. 启动MySQL数据库

[root@master ~]# docker container start mysql 

需要注意的是，数据库里的图片地址为我本地FastDFS图片服务器的地址，需要自个改为自己的图片服务器地址。然后导入movie.sql文件到MySQL数据库

2. 启动FastDFS图片服务器

FastDFS图片服务器安装和使用可参考我的博客：https://www.cnblogs.com/GH-123/p/10389453.html

网盘下载爬取的图片data.tar.bz2，解压放到FastDFS的数据文件夹里，然后启动FastDFS

[root@master ~]# docker container start fastdfs_storage 

[root@master ~]# docker container start fastdfs_tracker 

浏览器打开(注意替换为自个的IP和端口)：http://10.20.0.129:8888/group1/M00/00/00/ChQAgVy2nQ6AYSqBAAAqtGGbKlA116.jpg 能看到图片即正常。

3. 导入项目，在Maven的Movie下面运行tomcat7插件即可运行电影网站。

4. 在Maven的MovieManager下面运行tomcat7插件即可运行电影网站后台管理。

5. 数据清洗

(1) 启动Hadoop：[root@master ~]# /usr/local/zgh/hadoop/sbin/start-all.sh

(2) 启动Spark： [root@master ~]# /usr/local/zgh/spark/sbin/start-all.sh


(3) 启动Hive：  [root@master ~]# nohup hive --service metastore >> metastore.log 2>&1 &

(4) 上传jar包(spark-sql要与hive集成)：[root@master ~]# spark-submit --class com.zgh.datacleaner.ETL SparkMovie.jar --jars sparkMovie-1.0-SNAPSHOT-jar-with-dependencies.jar

数据清洗成功，成功于hive中建表，数据存储在hdfs中。

6. 数据加工

(1) 启动上述的服务

(2) 上传jar包：[root@master ~]# spark-submit --class com.zgh.datacleaner.RatingData SparkMovie.jar --jars sparkMovie-1.0-SNAPSHOT-jar-with-dependencies.jar

spark-sql提取数据中的用户id、电影id和评价，随机划分出80%为训练集和20%为测试集，并存储在hdfs上

7. 训练模型

(1) 启动上述的服务

(2) 上传jar包：[root@master ~]# spark-submit --class com.zgh.ml.ModelTraining2 SparkMovie.jar --jars sparkMovie-1.0-SNAPSHOT-jar-with-dependencies.jar

成功训练出模型，并存储在hdfs上。

8. 喜好预测并为用户推荐

(1) 启动上述的服务

(2) 上传jar包：[root@master ~]# spark-submit --class com.zgh.ml.Recommender SparkMovie.jar --jars sparkMovie-1.0-SNAPSHOT-jar-with-dependencies.jar

可输入用户并预测出用户的喜好

9. 数据入库

(1) 启动上述的服务

(2) 上传jar包：[root@master ~]# spark-submit --class com.zgh.ml.RecommendForAllUsers SparkMovie.jar --jars sparkMovie-1.0-SNAPSHOT-jar-with-dependencies.jar

为所有用户预测出喜好，并存储在MySQL中。

10. 实时预测

(1) 启动上述的服务

(2) 启动nginx：    [root@master ~]# nginx

(3) 启动zookeeper：[root@master ~]# nohup zookeeper-server-start.sh /usr/local/zgh/kafka/config/zookeeper.properties >> zookeeper.log 2>&1 &

(4) 启动kafka：    [root@master ~]# nohup kafka-server-start.sh /usr/local/zgh/kafka/config/server.properties >> kafka.log 2>&1 &

(5) 启动flume：    [root@master ~]# nohup flume-ng agent --conf /usr/local/zgh/flume/conf/ --conf-file /usr/local/zgh/flume/conf/flume-conf.properties --name a1 -Dflume.root.logger=INFO,console >> flume.log 2>&1 &

(6) 上传jar包：    [root@master ~]# spark-submit --class com.zgh.datacleaner.PopularMovies SparkMovie.jar --jars sparkMovie-1.0-SNAPSHOT-jar-with-dependencies.jar

用于为没有登录或新用户进行预测

(7) 上传jar包：    [root@master ~]# spark-submit --class com.zgh.streaming.SparkDrStreamALS SparkMovie.jar --jars sparkMovie-1.0-SNAPSHOT-jar-with-dependencies.jar

实时用户预测


#### 参与贡献

1. Fork 本仓库

2. 新建 Feat_xxx 分支

3. 提交代码

4. 新建 Pull Request



#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
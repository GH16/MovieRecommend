package com.dream;

import org.apache.commons.io.IOUtils;
import org.csource.fastdfs.*;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/*
 *  把在线图片转到本地
 *  HttpURLConnection 会比downloadPicture禁止ip的概率大
 * */

public class MovieImage1{

    // JDBC 驱动名及数据库 URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_URL = "jdbc:mysql://10.20.0.129:3306/movie";

    // 数据库的用户名与密码，需要根据自己的设置
    static final String USER = "root";
    static final String PASS = "123456";

    public static void main(String[] args) throws Exception {

        Connection conn = null;
        Statement stmt = null;
        Statement stmt1 = null;

        // 注册 JDBC 驱动
        Class.forName("com.mysql.jdbc.Driver");

        // 打开链接
        System.out.println("连接数据库...");
        conn = DriverManager.getConnection(DB_URL,USER,PASS);

        // 执行查询
        System.out.println(" 实例化Statement对象...");
        stmt = conn.createStatement();
        stmt1 = conn.createStatement();
        String sql;
        sql = "SELECT movieid, picture, backpost FROM movie";
        ResultSet rs = stmt.executeQuery(sql);

        ClientGlobal.init("E:\\project\\IdeaProjects\\MovieRecommend\\Movie\\src\\main\\resources\\fdfs_client.properties");
        TrackerClient trackerClient = new TrackerClient();
        TrackerServer trackerServer = trackerClient.getConnection();
        StorageServer storageServer = null;
        StorageClient storageClient = new StorageClient(trackerServer, storageServer);

        String sql1;
        sql1 = "UPDATE movie SET movie1 WHERE movieid=movieid1";
        while (rs.next()){
            if (rs.getString("picture").contains("jpg") && !rs.getString("picture").contains("8888")){
                String image = urlToDFS(rs.getString("picture"), storageClient);
                String sql2 = sql1.replaceFirst("movie1", "picture='http://10.20.0.129:8888/group1/" + image + "'").replaceFirst("movieid1", rs.getString("movieid"));
                System.out.println(rs.getString("movieid"));
                stmt1.execute(sql2);
            }
            if (rs.getString("backpost").contains("jpg") && !rs.getString("backpost").contains("8888")){
                String image = urlToDFS(rs.getString("backpost"), storageClient);
                String sql2 = sql1.replaceFirst("movie1", "backpost='http://10.20.0.129:8888/group1/" + image + "'").replaceFirst("movieid1", rs.getString("movieid"));
                stmt1.execute(sql2);
            }
        }

        // 完成后关闭
        rs.close();
        stmt.close();
        stmt1.close();
        conn.close();
    }

    public static String urlToDFS(String url1, StorageClient storageClient) throws Exception {
        URL url = new URL(url1);
        HttpURLConnection connUrl = (HttpURLConnection)url.openConnection();
        //通过输入流获取图片数据
        InputStream inputStream = connUrl.getInputStream();
        String[] storePath= storageClient.upload_file("group1", IOUtils.toByteArray(inputStream),"jpg",null);
        inputStream.close();
        return storePath[1];
    }

}

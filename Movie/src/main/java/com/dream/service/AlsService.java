package com.dream.service;

import com.dream.po.Movie;

import java.util.List;

/**
 * Created by zgh on 2019/3/29.
 */


public interface AlsService {
    List<Movie> selectAlsMoviesByUserId(Integer userid);
}
